#include "Actuator.h"

Actuator::Actuator(string key, string value)
	: key(key)
	, value(value)
{
}

void Actuator::setValue(string value)
{
	this->value = value;
}

string Actuator::getValue()
{
	return value;
}

string Actuator::getKey()
{
	return key;
}