#include <ESP8266WiFi.h>
#include <Wire.h>
#include <string.h>
#include <cstring>
#include <FastLED.h>

// Set WiFi credentials
#define WIFI_SSID "Team Rocket Internet Portable"
#define WIFI_PASS "HDJ2IO5Z8LBWw"
#define HOST "10.0.42.1"
#define PORT 8888
#define ID 115 // Device ID = last digits of IP (so < 255)
#define NUM_LEDS 1
#define LED_PIN D5

void readMotionSensor();
void socket_connect();
void toggleLed();
void sendNewData();

WiFiClient client;

String incomingData;
String sensorDataString;

int pir_state = 0;
int last_state; 
char pirBuffer[2];

CRGB leds[NUM_LEDS];

struct Data
{
	String key;
	String value;
}motionSensor, tableLampLed;

void setup() {
	Wire.begin();
	// Setup serial port
	Serial.begin(115200);
	Serial.setTimeout(50);
	Serial.println();
	 
	// Begin WiFi
	IPAddress ip(10, 0, 42, ID);
	IPAddress gateway(10, 0, 42, 1);
	IPAddress subnet(255, 255, 255, 0);
	WiFi.config(ip, gateway, subnet);
	WiFi.begin(WIFI_SSID, WIFI_PASS);
	
	// Connecting to WiFi...
	Serial.print("Connecting to ");
	Serial.print(WIFI_SSID);
	// Loop continuously while WiFi is not connected
	while(WiFi.status() != WL_CONNECTED)
	{
		delay(100);
		Serial.print(".");
	}
 
	// Connected to WiFi
	Serial.println();
	Serial.print("Connected! IP address: ");
	Serial.println(WiFi.localIP());
	
	pinMode(LED_PIN, OUTPUT);
	FastLED.addLeds<WS2812B, LED_PIN>(leds, NUM_LEDS);
	
	motionSensor.key = "tableLampMotion";
	motionSensor.value = "0";
	tableLampLed.key = "tableLampLed";
	tableLampLed.value = "0";

	// Socket Connection
	socket_connect();
}

void loop() {
	while (client.connected())
	{
		while (client.available()) // Returns the number of bytes available for reading
			{
				incomingData = client.readStringUntil('\n');  // Read the next byte recieved from the pi
			}
		toggleLed();
		readMotionSensor();
		sendNewData();
		delay(10);  // To make sensors more reliable
	}
}

void socket_connect() {
	// Connect to the websocket server
	if(client.connect(HOST, PORT)) {
		Serial.println("Connected to server.");
	}else {
		Serial.println("Connection to server failed...");
		delay(1000);
		ESP.restart();
	}
}

void readMotionSensor()
{
	Wire.beginTransmission(0x38);
	Wire.write(byte(0x00));
	Wire.endTransmission();
	
	Wire.requestFrom(0x38, 1);
	pir_state = Wire.read();

	if (pir_state % 2 == 1)
	{
		motionSensor.value = "1";
	}
	else if (pir_state % 2 == 0)
	{
		motionSensor.value = "0";
	}
}

void toggleLed()
{
	if (incomingData == "ledOn") {
		leds[0] = CRGB::AntiqueWhite;
		FastLED.show();
		tableLampLed.value = "1";
	}
	else if (incomingData == "ledOff") {
		leds[0] = CRGB::Black;
		FastLED.show();
		tableLampLed.value = "0";
	}
}

void sendNewData()
{
	sensorDataString = motionSensor.key + "," + motionSensor.value + ";" + tableLampLed.key + "," + tableLampLed.value + ";";
	char sensorDataBuffer[60];
	strcpy(sensorDataBuffer, sensorDataString.c_str());
	client.write(sensorDataBuffer);	
}
