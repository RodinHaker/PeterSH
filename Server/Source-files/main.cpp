/**
 * Boots the server.
 * Allows for multiple connections at once through multithreading.
 *
 * @file main.cpp
 * @authors PJSDV Class D.1 Group 2
 * @version 1.0 27/11/2020
*/
#include "Database.h"
#include "Server.h"
#include <stdio.h>
#include <iostream> // for cout and and endl

using namespace std;

int main(int argc, char* argv[])
{
	Database *database = new Database();
	Server server(database);
	server.start();
}