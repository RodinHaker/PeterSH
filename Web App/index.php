<?php
    include 'php/db_con.php'
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Home Assistant">
    <link rel="icon" type="image/png" href="images/icons/favicon.ico">
    <link rel="stylesheet" href="css/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="js/main.js"></script>
</head>
<body>
    <div id="container">
        <header id="header">
            <div id="left">
                <div id="logo">Home Assistant</div>
            </div>
        </header>
        <main id="main">
            <div id="actions-container">
                <div id="dashboard-action">
                    <div class="title">Home</div>
                    <div class="row">
                        <div class="action">
                            <div id="test"></div>
                        </div>
                        <div class="action">
                            <div id="lights"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="action"></div>
                        <div class="action"></div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</body>
</html>