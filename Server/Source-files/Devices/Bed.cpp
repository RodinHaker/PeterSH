#include "Bed.h"

Bed::Bed(string name, int sockfd) 
	: Device(name, sockfd)
{
	forceSensor = new Sensor("bedForce", "0");
	pushButton = new Sensor("bedButton", "0");
	led = new Actuator("bedLed", "0");
	addSensor(forceSensor);
	addSensor(pushButton);
	addActuator(led);
}

Bed::~Bed() {
	delete forceSensor;
	delete pushButton;
	delete led;
}

void *Bed::deviceThread(void *device)
{
	Bed *bed = (Bed *)device;
	cout << "Bed thread " << (int)bed->tid << " starting...\n" << endl;
	char* data;
	int triggered1 = 0, triggered2 = 0;
	while (1) {	
		bed->connection(bed);
		
		string str;
		char c[20];
		int force = stoi(bed->forceSensor->getValue());
		if ((force > 500) && !triggered1)
		{
			cout << "Peter ligt in bed!" << endl;
			triggered1 = 1;
		}
		else if ((force <= 500) && triggered1)
		{
			cout << "Peter is uit bed!" << endl;
			triggered1 = 0;
		}
		
		if ((stoi(bed->pushButton->getValue()) == 1) && !triggered2)
		{
			str = "ledOn";
			strcpy(c, str.c_str());
			bed->sendData(bed->sockfd, c);
			triggered2 = 1;
		}
		else if ((stoi(bed->pushButton->getValue()) <= 500) && triggered2)
		{
			str = "ledOff";
			strcpy(c, str.c_str());
			bed->sendData(bed->sockfd, c);
			triggered2 = 0;
		}
	}	

	printf("Exit bed thread\n\r");	
	pthread_exit(NULL);

	return 0;
}

void Bed::start()
{
	pthread_mutex_lock(&qlock);	
	if (pthread_create(&(this->tid), 
		NULL,
		Bed::deviceThread,
		(void *)this) < 0)
	{
		pthread_mutex_unlock(&qlock);	
		cerr << "Error creating bed thread!" << endl;
		return;
	}
	pthread_mutex_unlock(&qlock);	
}