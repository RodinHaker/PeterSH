#pragma once

#include <ctime>
#include <iostream>
#include "Device.h"

using namespace std;

class Fridge : public Device
{
public:
	Fridge(string name, int sockfd);
	virtual ~Fridge();
	void start();
private:
	static void *deviceThread(void *device);
	Sensor* doorSwitch;
	Sensor* tempSensor1;
	Sensor* tempSensor2;
	Actuator* fan;
	Actuator* peltier;
	time_t timeStart;
};