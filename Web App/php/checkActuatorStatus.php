<?php
    include 'db_con.php';

    $sql = "SELECT * FROM actuators";
    $result = mysqli_query($conn, $sql);
    if(mysqli_num_rows($result) > 0){
        while($row = mysqli_fetch_assoc($result)){
            echo "<div id=\"actuator-sliders\">";
            echo "<p>$row[vkey]: </p>";
            echo "<label class=\"switch switch-left-right\">";
            echo "<input class=\"switch-input\" id=\"$row[vkey]\" type=\"checkbox\" value=\"$row[vkey]\" ".($row[value] == 1 ? "checked":"").">";
            echo "<span class=\"switch-label\" id=\"$row[vkey]Label\" data-on=\"On\" data-off=\"Off\"></span>";
            echo "<span class=\"switch-handle\" id=\"$row[vkey]Handle\"></span>";
            echo "</label>";
            echo "</div>";
            echo "<br>";
        }
    }else{
        echo "There are no actuators!";
    }