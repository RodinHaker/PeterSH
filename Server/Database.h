#pragma once

#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <streambuf>
#include <string>
#include <list>
#include "mysql_connection.h"
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include "Device.h"

using namespace sql;
using namespace std;

class Device;

class Database
{
public :
	Database();
	~Database();
	void createConnection();
	void createDatabase();
	void addDevice(Device*);
	void updateDatabase();
	void updateSensorData(string, string);
	void updateActuatorData(string, string);
	list<Device*> *getDevices();
	string readActuatorData(string);
private:
	list<Device*> *devices;
	Driver *driver;
	Connection *con;
	Statement *stmt;
	ResultSet *res;
	string host;
	string uname;
	string passwd;
	string database;
};