#pragma once

#include <ctime>
#include <iostream>
#include "Device.h"

using namespace std;

class TableLamp : public Device
{
public:
	TableLamp(string name, int sockfd);
	virtual ~TableLamp();
	void start();
private:
	static void *deviceThread(void *device);
	Sensor* motionSensor;
	Actuator* RGBLed;
};