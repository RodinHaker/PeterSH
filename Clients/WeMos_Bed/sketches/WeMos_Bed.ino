#include <ESP8266WiFi.h>
#include <Wire.h>
#include <string.h>
#include <cstring>

// Set WiFi credentials
#define WIFI_SSID "Team Rocket Internet Portable"
#define WIFI_PASS "HDJ2IO5Z8LBWw"
#define HOST "10.0.42.1"
#define PORT 8888
#define ID 109 // Device ID = last digits of IP (so < 255)

void readForceSensor();
void readButton();
void socket_connect();
void toggleLed();
void sendNewData();

WiFiClient client;

String incomingData;
String sensorDataString;

int button_state = 0;
int last_state;
char pushButtonBuffer[2];

unsigned int forceSensorIn = 0;
char forceSensorBuffer[20];

struct Data
{
	String key;
	String value;
}forceSensor, pushButton, chairLed;

void setup() {
	Wire.begin();
	// Setup serial port
	Serial.begin(115200);
	Serial.setTimeout(50);
	Serial.println();
	 
	// Begin WiFi
	IPAddress ip(10, 0, 42, ID);
	IPAddress gateway(10, 0, 42, 1);
	IPAddress subnet(255, 255, 255, 0);
	WiFi.config(ip, gateway, subnet);
	WiFi.begin(WIFI_SSID, WIFI_PASS);
	
	// Connecting to WiFi...
	Serial.print("Connecting to ");
	Serial.print(WIFI_SSID);
	// Loop continuously while WiFi is not connected
	while(WiFi.status() != WL_CONNECTED)
	{
		delay(100);
		Serial.print(".");
	}
 
	// Connected to WiFi
	Serial.println();
	Serial.print("Connected! IP address: ");
	Serial.println(WiFi.localIP());

	forceSensor.key = "bedForce";
	forceSensor.value = "0";
	pushButton.key = "bedButton";
	pushButton.value = "0";
	chairLed.key = "bedLed";
	chairLed.value = "0";

	// Socket Connection
	socket_connect();
}

void loop() {
	while (client.connected())
	{
		while (client.available()) // Returns the number of bytes available for reading
		{
			incomingData = client.readStringUntil('\n');  // Read the next byte recieved from the pi
		}
		toggleLed();
		readForceSensor();
		readButton();
		sendNewData();
		delay(10); // To make sensors more reliable
	}
}

void socket_connect() {
	// Connect to the websocket server
	if(client.connect(HOST, PORT)) {
		Serial.println("Connected to server.");
	}else {
		Serial.println("Connection to server failed...");
		delay(1000);
		ESP.restart();
	}
}

void readForceSensor()
{
	//Read analog 10bit inputs 0&1
	Wire.requestFrom(0x36, 2);  
	forceSensorIn = Wire.read() & 0x03;  
	forceSensorIn <<= 8;
	forceSensorIn |= Wire.read();  
	itoa(forceSensorIn, forceSensorBuffer, 10);
	forceSensor.value = forceSensorBuffer;
}

void readButton()
{
	last_state = button_state;
	Wire.beginTransmission(0x38);
	Wire.write(byte(0x00));
	Wire.endTransmission();
	
	Wire.requestFrom(0x38, 1);
	button_state = Wire.read();

	if (last_state%2 == 1 && button_state%2 == 0)
	{
		int cur_value = pushButton.value.toInt();
		int new_value = (cur_value + 1) % 2;
		itoa(new_value, pushButtonBuffer, 10);
		pushButton.value = pushButtonBuffer;
		Serial.println(pushButtonBuffer);
	}
}

void toggleLed()
{
	Wire.beginTransmission(0x38);
	Wire.write(byte(0x03));
	Wire.write(byte(0x0F));
	Wire.endTransmission();
	
	if (incomingData == "ledOff") {
		Wire.beginTransmission(0x38);
		Wire.write(byte(0x01));
		Wire.write(byte(0 << 4));
		Wire.endTransmission();
		chairLed.value = "0";
	}
	else if (incomingData == "ledOn") {
		Wire.beginTransmission(0x38);
		Wire.write(byte(0x01));
		Wire.write(byte(1 << 4));
		Wire.endTransmission();
		chairLed.value = "1";
	}
}

void sendNewData()
{
	sensorDataString = forceSensor.key + "," + forceSensor.value + ";" + pushButton.key + "," + pushButton.value + ";" + chairLed.key + "," + chairLed.value + ";";
	char sensorDataBuffer[60];
	strcpy(sensorDataBuffer, sensorDataString.c_str());
	client.write(sensorDataBuffer);	
}
