#include "Column.h"

Column::Column(string name, int sockfd) 
	: Device(name, sockfd)
{
	pushButton = new Sensor("columnButton", "0");
	gasSensor = new Sensor("columnGas", "0");
	buzzer = new Actuator("columnBuzzer", "0");
	led = new Actuator("columnLed", "0");
	addSensor(pushButton);
	addSensor(gasSensor);	
	addActuator(buzzer);
	addActuator(led);	
}

Column::~Column() {
	delete pushButton;
	delete gasSensor;
	delete buzzer;
	delete led;
}

void *Column::deviceThread(void *device)
{
	Column *column = (Column *)device;
	cout << "Column thread " << (int)column->tid << " starting...\n" << endl;
	char* data;
	int triggered1 = 0, triggered2 = 0, cancelled = 0;
	while (1) {	
		column->connection(column);
		
		string str;
		char c[20];
		if ((stoi(column->gasSensor->getValue()) < 400) && !triggered1 && !cancelled)
		{
			str = "buzzerOn";
			strcpy(c, str.c_str());
			column->sendData(column->sockfd, c);
			str = "ledOn";
			strcpy(c, str.c_str());
			column->sendData(column->sockfd, c);
			triggered1 = 1;
		}
		else if ((stoi(column->gasSensor->getValue()) >= 400) && triggered1)
		{
			str = "buzzerOff";
			strcpy(c, str.c_str());
			column->sendData(column->sockfd, c);
			str = "ledOff";
			strcpy(c, str.c_str());
			column->sendData(column->sockfd, c);
			triggered1 = 0;
			cancelled = 0;
		}
		
		if ((stoi(column->pushButton->getValue())%2 == 1) && !triggered2)
		{
			str = "buzzerOff";
			strcpy(c, str.c_str());
			column->sendData(column->sockfd, c);
			str = "ledOff";
			strcpy(c, str.c_str());
			column->sendData(column->sockfd, c);
			triggered2 = 1;
			cancelled = 1;
		}
		else if ((stoi(column->pushButton->getValue())%2 == 0) && triggered2)
		{
			str = "buzzerOff";
			strcpy(c, str.c_str());
			column->sendData(column->sockfd, c);
			str = "ledOff";
			strcpy(c, str.c_str());
			column->sendData(column->sockfd, c);
			triggered2 = 0;
			cancelled = 1;
		}
	}
	printf("Exit column thread\n\r");	
	pthread_exit(NULL);

	return 0;
}

void Column::start()
{
	pthread_mutex_lock(&qlock);	
	if (pthread_create(&(this->tid), 
		NULL,
		Column::deviceThread,
		(void *)this) < 0)
	{
		pthread_mutex_unlock(&qlock);	
		cerr << "Error creating column thread!" << endl;
		return;
	}
	pthread_mutex_unlock(&qlock);	
}