#include <ESP8266WiFi.h>
#include <Wire.h>
#include <string.h>
#include <cstring>

// Set WiFi credentials
#define WIFI_SSID "Team Rocket Internet Portable"
#define WIFI_PASS "HDJ2IO5Z8LBWw"
#define HOST "10.0.42.1"
#define PORT 8888
#define ID 112 // Device ID = last digits of IP (so < 255)
#define PELTIER_PIN D5

void socket_connect();
void readTempSensors();
void readDoor();
void toggleFan();
void togglePeltier();
void sendNewData();

WiFiClient client;

String incomingData;
String sensorDataString;

int switch_state = 0;
char switchBuffer[2];

unsigned int tempSensorIn1 = 0;
unsigned int tempSensorIn2 = 0;
char tempSensorBuffer1[20];
char tempSensorBuffer2[20];

struct Data
{
	String key;
	String value;
}tempSensor1, tempSensor2, fridgeDoor, fan, peltier;

void setup() {
	Wire.begin();
	// Setup serial port
	Serial.begin(115200);
	Serial.setTimeout(50);
	Serial.println();
	 
	// Begin WiFi
	IPAddress ip(10, 0, 42, ID);
	IPAddress gateway(10, 0, 42, 1);
	IPAddress subnet(255, 255, 255, 0);
	WiFi.config(ip, gateway, subnet);
	WiFi.begin(WIFI_SSID, WIFI_PASS);
	
	// Connecting to WiFi...
	Serial.print("Connecting to ");
	Serial.print(WIFI_SSID);
	// Loop continuously while WiFi is not connected
	while(WiFi.status() != WL_CONNECTED)
	{
		delay(100);
		Serial.print(".");
	}
 
	// Connected to WiFi
	Serial.println();
	Serial.print("Connected! IP address: ");
	Serial.println(WiFi.localIP());

	tempSensor1.key = "fridgeTemp1";
	tempSensor1.value = "0";
	tempSensor2.key = "fridgeTemp2";
	tempSensor2.value = "0";
	fridgeDoor.key = "fridgeDoor";
	fridgeDoor.value = "0";
	fan.key = "fridgeFan";
	fan.value = "0";
	peltier.key = "fridgePeltier";
	peltier.value = "0";

	pinMode(PELTIER_PIN, OUTPUT);
	
	// Socket Connection
	socket_connect();
}

void loop() {
	while (client.connected())
	{
		while (client.available()) // Returns the number of bytes available for reading
			{
				incomingData = client.readStringUntil('\n');  // Read the next byte recieved from the pi
			}
		readTempSensors();
		readDoor();
		toggleFan();
		togglePeltier();
		sendNewData();
		delay(10);  // To make sensors more reliable
	}
}

void socket_connect() {
	// Connect to the websocket server
	if(client.connect(HOST, PORT)) {
		Serial.println("Connected to server.");
	}else {
		Serial.println("Connection to server failed...");
		delay(1000);
		ESP.restart();
	}
}

void readTempSensors()
{
	//Read analog 10bit inputs 0&1
	Wire.requestFrom(0x36, 4);  
	tempSensorIn1 = Wire.read() & 0x03;  
	tempSensorIn1 <<= 8;
	tempSensorIn1 |= Wire.read();  
	tempSensorIn2 = Wire.read() & 0x03;    
	tempSensorIn2 <<= 8;                        
	tempSensorIn2 |= Wire.read();
	itoa(tempSensorIn1, tempSensorBuffer1, 10);
	itoa(tempSensorIn2, tempSensorBuffer2, 10);
	tempSensor1.value = tempSensorBuffer1;
	tempSensor2.value = tempSensorBuffer2;
}

void readDoor()
{
	Wire.beginTransmission(0x38);
	Wire.write(byte(0x00));
	Wire.endTransmission();
	Wire.requestFrom(0x38, 1);
	switch_state = Wire.read();
	itoa(switch_state, switchBuffer, 10);
	fridgeDoor.value = switchBuffer;
}

void toggleFan()
{
	Wire.beginTransmission(0x38);
	Wire.write(byte(0x03));
	Wire.write(byte(0x0F));
	Wire.endTransmission();
	
	if (incomingData == "fanOff") {
		Wire.beginTransmission(0x38);
		Wire.write(byte(0x01));
		Wire.write(byte(0 << 4));
		Wire.endTransmission();
		fan.value = "0";
	}
	else if (incomingData == "fanOn") {
		Wire.beginTransmission(0x38);
		Wire.write(byte(0x01));
		Wire.write(byte(1 << 4));
		Wire.endTransmission();
		fan.value = "1";
	}
}

void togglePeltier()
{	
	if (incomingData == "peltierOff") {
		digitalWrite(PELTIER_PIN, LOW);
		fan.value = "0";
	}
	else if (incomingData == "peltierOn") {
		digitalWrite(PELTIER_PIN, HIGH);
		fan.value = "1";
	}
}

void sendNewData()
{
	sensorDataString = tempSensor1.key + "," + tempSensor1.value + ";"
					+ tempSensor2.key + "," + tempSensor2.value + ";"
					+ fridgeDoor.key + "," + fridgeDoor.value + ";"
					+ fan.key + "," + fan.value + ";"
					+ peltier.key + "," + peltier.value + ";";
	char sensorDataBuffer[60];
	strcpy(sensorDataBuffer, sensorDataString.c_str());
	client.write(sensorDataBuffer);	
}
