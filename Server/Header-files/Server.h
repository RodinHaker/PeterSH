#pragma once

#include <string.h>
#include <iostream> 
#include <map>
#include <string>
#include <string.h>
#include <pthread.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <readline/readline.h>
#include <readline/history.h>

#include "Database.h"
#include "Device.h"
#include "Bed.h"
#include "Column.h"
#include "Chair.h"
#include "Fridge.h"
#include "Wall.h"
#include "Door.h"
#include "TableLamp.h"

#define LISTEN_BACKLOG 10

using namespace std;

class Database;

/**
 * Creates and manages connections with the devices
*/
class Server
{
public:
	Server(Database *database);
	void start();
	static void startDevice(Server*, Device*, string, struct sockaddr );
	pthread_t getThread();
	pthread_mutex_t mlock = PTHREAD_MUTEX_INITIALIZER;
	//list<Device*> devices;
private:
	Database *database;
	pthread_t thread;
	pthread_t cli_thread;
	pthread_t database_thread;
	int portnm = 8888;
	// function running inside the communication thread
	// to open the socket - called by listen
	static void *socketThread(void *socket);
	static void *cliThread(void *socket);
	static void *databaseThread(void *socket);

	// socket file descriptor
	int sockfd;

	// data for incoming socket
	struct sockaddr_in sock_addr;
	
	bool quit;
};