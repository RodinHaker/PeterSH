#include <ESP8266WiFi.h>
#include <Wire.h>
#include <string.h>
#include <cstring>
#include <FastLED.h>

// Set WiFi credentials
#define WIFI_SSID "Team Rocket Internet Portable"
#define WIFI_PASS "HDJ2IO5Z8LBWw"
#define HOST "10.0.42.1"
#define PORT 8888
#define ID 113 // Device ID = last digits of IP (so < 255)
#define LED_PIN D5
#define NUM_LEDS 3

void socket_connect();
void readLightAndPot();
void lcdPanel();
void toggleLed();
void sendNewData();

WiFiClient client;

String incomingData;
String sensorDataString;

int button_state = 0;
int last_state; 
char pushButtonBuffer[2];

unsigned int potMeterIn = 0;
unsigned int ldrIn = 0;
char potMeterBuffer[20];
char ldrBuffer[20];
int ledsAan = 0;

CRGB leds[NUM_LEDS];

struct Data
{
	String key;
	String value;
}lightSensor, pot_meter, wallLed, wallLCD;

void setup() {
	Wire.begin();
	// Setup serial port
	Serial.begin(115200);
	Serial.setTimeout(50);
	Serial.println();
	 
	// Begin WiFi
	IPAddress ip(10, 0, 42, ID);
	IPAddress gateway(10, 0, 42, 1);
	IPAddress subnet(255, 255, 255, 0);
	WiFi.config(ip, gateway, subnet);
	WiFi.begin(WIFI_SSID, WIFI_PASS);
	
	// Connecting to WiFi...
	Serial.print("Connecting to ");
	Serial.print(WIFI_SSID);
	// Loop continuously while WiFi is not connected
	while(WiFi.status() != WL_CONNECTED)
	{
		delay(100);
		Serial.print(".");
	}
 
	// Connected to WiFi
	Serial.println();
	Serial.print("Connected! IP address: ");
	Serial.println(WiFi.localIP());

	lightSensor.key = "wallLightSensor";
	lightSensor.value = "0";
	pot_meter.key = "wallPotMeter";
	pot_meter.value = "0";
	wallLed.key = "wallLed";
	wallLed.value = "0";
	wallLCD.key = "wallLCD";
	wallLCD.value = "0";
	
	FastLED.addLeds<WS2812B, LED_PIN>(leds, NUM_LEDS);

	// Socket Connection
	socket_connect();
}

void loop() {
	while (client.connected())
	{
		while (client.available()) // Returns the number of bytes available for reading
		{
			incomingData = client.readStringUntil('\n');  // Read the next byte recieved from the pi
		}
		readLightAndPot();
		lcdPanel();
		toggleLed();
		sendNewData();
		incomingData = "";
		delay(10);  // To make sensors more reliable
	}
}

void socket_connect() {
	// Connect to the websocket server
	if(client.connect(HOST, PORT)) {
		Serial.println("Connected to server.");
	}else {
		Serial.println("Connection to server failed...");
		delay(1000);
		ESP.restart();
	}
}

void readLightAndPot()
{
	//Read analog 10bit inputs 0&1
	Wire.requestFrom(0x36, 4);  
	ldrIn = Wire.read() & 0x03;  
	ldrIn <<= 8;
	ldrIn |= Wire.read();  
	potMeterIn = Wire.read() & 0x03;    
	potMeterIn <<= 8;                        
	potMeterIn |= Wire.read();   // rotary encoder
	itoa(ldrIn, ldrBuffer, 10);
	itoa(potMeterIn, potMeterBuffer, 10);
	lightSensor.value = ldrBuffer;
	pot_meter.value = potMeterBuffer;
}

void toggleLed()
{
	if ((incomingData == "ledOn" || ledsAan == 1) && incomingData != "ledOff") {
		leds[0] = CRGB::AntiqueWhite;
		leds[1] = CRGB::AntiqueWhite;
		leds[2] = CRGB::AntiqueWhite;
		FastLED.setBrightness(pot_meter.value.toInt()/4);
		Serial.println("leds aan...");
		ledsAan = 1;
		wallLed.value = "1";
	}
	else if (incomingData == "ledOff") {
		leds[0] = CRGB::Black;
		leds[1] = CRGB::Black;
		leds[2] = CRGB::Black;
		ledsAan = 0;
		Serial.println("leds uit...");
		wallLed.value = "0";
	}
}

void lcdPanel()
{
	Wire.beginTransmission(0x38);
	Wire.write(byte(0x03));
	Wire.write(byte(0x0F));
	Wire.endTransmission();

	if (incomingData == "lcdOn")
	{
		Wire.beginTransmission(0x38);
		Wire.write(byte(0x01));
		Wire.write(byte(1 << 4));
		Wire.endTransmission();
		wallLCD.value = "1";
	}
	else if (incomingData == "lcdOff")
	{
		Wire.beginTransmission(0x38);
		Wire.write(byte(0x01));
		Wire.write(byte(0 << 4));
		Wire.endTransmission();
		wallLCD.value = "0";
	}
}

void sendNewData()
{
	sensorDataString = lightSensor.key + "," + lightSensor.value + ";"
					+ pot_meter.key + "," + pot_meter.value + ";"
					+ wallLed.key + "," + wallLed.value + ";"
					+ wallLCD.key + "," + wallLCD.value + ";";
	char sensorDataBuffer[60];
	strcpy(sensorDataBuffer, sensorDataString.c_str());
	client.write(sensorDataBuffer);	
}
