#include "Sensor.h"

Sensor::Sensor(string key, string value)
	: key(key)
	, value(value)
{
}

void Sensor::setValue(string value)
{
	this->value = value;
}

const string &Sensor::getValue()
{
	return value;
}

const string &Sensor::getKey()
{
	return key;
}
