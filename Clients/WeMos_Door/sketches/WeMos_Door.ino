#include <ESP8266WiFi.h>
#include <Wire.h>
#include <string.h>
#include <cstring>
#include <Servo.h>

// Set WiFi credentials
#define WIFI_SSID "Team Rocket Internet Portable"
#define WIFI_PASS "HDJ2IO5Z8LBWw"
#define HOST "10.0.42.1"
#define PORT 8888
#define ID 114 // Device ID = last digits of IP (so < 255)
#define DOOR_PIN D5

void socket_connect();
void readButtons();
void toggleLeds();
void toggleDoor();
void sendNewData();

WiFiClient client;

String incomingData;
String sensorDataString;

int button_states = 0;
int last_states; 
char pushButtonsBuffer[2];

Servo door;
int open = 0;
int closed = 90;

struct Data
{
	String key;
	String value;
}pushButton1, pushButton2, doorLed1, doorLed2, servo;

void setup() {
	Wire.begin();
	// Setup serial port
	Serial.begin(115200);
	Serial.setTimeout(50);
	Serial.println();
	 
	// Begin WiFi
	IPAddress ip(10, 0, 42, ID);
	IPAddress gateway(10, 0, 42, 1);
	IPAddress subnet(255, 255, 255, 0);
	WiFi.config(ip, gateway, subnet);
	WiFi.begin(WIFI_SSID, WIFI_PASS);
	
	// Connecting to WiFi...
	Serial.print("Connecting to ");
	Serial.print(WIFI_SSID);
	// Loop continuously while WiFi is not connected
	while(WiFi.status() != WL_CONNECTED)
	{
		delay(100);
		Serial.print(".");
	}
 
	// Connected to WiFi
	Serial.println();
	Serial.print("Connected! IP address: ");
	Serial.println(WiFi.localIP());

	pushButton1.key = "doorButton1";
	pushButton1.value = "0";
	pushButton2.key = "doorButton2";
	pushButton2.value = "0";
	doorLed1.key = "doorLed1";
	doorLed1.value = "0";
	doorLed2.key = "doorLed2";
	doorLed2.value = "0";
	servo.key = "doorServo";
	servo.value = "0";
	
	door.attach(DOOR_PIN);
	
	// Socket Connection
	socket_connect();
}

void loop() {
	while (client.connected())
	{
		while (client.available()) // Returns the number of bytes available for reading
		{
			incomingData = client.readStringUntil('\n');  // Read the next byte recieved from the pi
		}
		readButtons();
		toggleLeds();
		toggleDoor();
		sendNewData();
		delay(10);  // To make sensors more reliable
	}
}

void socket_connect() {
	// Connect to the websocket server
	if(client.connect(HOST, PORT)) {
		Serial.println("Connected to server.");
	}else {
		Serial.println("Connection to server failed...");
		delay(1000);
		ESP.restart();
	}
}

void readButtons()
{
	last_states = button_states;
	Wire.beginTransmission(0x38);
	Wire.write(byte(0x00));
	Wire.endTransmission();
	
	Wire.requestFrom(0x38, 1);
	button_states = Wire.read();
	if ((last_states == 1 || last_states == 2 || last_states == 3
		|| last_states == 49 || last_states == 50 || last_states == 51) && (button_states == 0 || button_states == 48))
	{
		int cur_value = pushButton1.value.toInt();
		int new_value = (cur_value + 1) % 2;
		itoa(new_value, pushButtonsBuffer, 10);
		pushButton1.value = pushButtonsBuffer;
	}
}

void toggleLeds()
{
	Wire.beginTransmission(0x38);
	Wire.write(byte(0x03));
	Wire.write(byte(0x0F));
	Wire.endTransmission();
	
	if (incomingData == "ledsOff") {
		Wire.beginTransmission(0x38);
		Wire.write(byte(0x01));
		Wire.write(byte(0));
		Wire.endTransmission();
		doorLed1.value = "0";
		doorLed2.value = "0";
	}
	else if (incomingData == "ledsOn") {
		Wire.beginTransmission(0x38);
		Wire.write(byte(0x01));
		Wire.write(byte(3 << 4));
		Wire.endTransmission();
		doorLed1.value = "1";
		doorLed2.value = "1";
	}
}

void toggleDoor()
{
	if (incomingData == "doorClose") {
		door.write(closed);
		servo.value = "0";
	}
	else if (incomingData == "doorOpen") {
		door.write(closed);
		servo.value = "1";
	}
}

void sendNewData()
{
	sensorDataString = pushButton1.key + "," + pushButton1.value + ";"
					 + pushButton2.key + "," + pushButton2.value + ";"
					 + doorLed1.key + "," + doorLed1.value + ";"
					 + doorLed2.key + "," + doorLed2.value + ";"
					 + servo.key + "," + servo.value + ";";
	char sensorDataBuffer[60];
	strcpy(sensorDataBuffer, sensorDataString.c_str());
	client.write(sensorDataBuffer);	
}
