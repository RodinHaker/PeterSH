#pragma once

#include <ctime>
#include <iostream>
#include "Device.h"

using namespace std;

class Column : public Device
{
public:
	Column(string name, int sockfd);
	virtual ~Column();
	void start();
private:
	static void *deviceThread(void *device);
	Sensor* pushButton;
	Sensor* gasSensor;
	Actuator* buzzer;
	Actuator* led;
};