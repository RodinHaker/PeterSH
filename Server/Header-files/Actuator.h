#pragma once

#include <iostream>

using namespace std;

class Actuator
{
public:
	Actuator(string key, string value);
	void setValue(string value);
	string getValue();
	string getKey();
private:
	string key;
	string value = 0;
};