#include "Fridge.h"

Fridge::Fridge(string name, int sockfd) 
	: Device(name, sockfd)
{
	doorSwitch = new Sensor("fridgeDoor", "0");
	tempSensor1 = new Sensor("fridgeTemp1", "0");
	tempSensor2 = new Sensor("fridgeTemp2", "0");
	fan = new Actuator("fridgeFan", "0");
	peltier = new Actuator("fridgePeltier", "0");
	addSensor(doorSwitch);
	addSensor(tempSensor1);	
	addSensor(tempSensor2);	
	addActuator(fan);
	addActuator(peltier);
}

Fridge::~Fridge() {
	delete doorSwitch;
	delete tempSensor1;
	delete tempSensor2;
	delete fan;
	delete peltier;
}

void *Fridge::deviceThread(void *device)
{
	Fridge *fridge = (Fridge *)device;
	cout << "Fridge thread " << (int)fridge->tid << " starting...\n" << endl;
	char* data;
	int triggered = 0, triggered2 = 0;
	while (1) {	
		fridge->connection(fridge);
		
		string str;
		char c[20];
		if ((stoi(fridge->tempSensor1->getValue()) > 300) && (stoi(fridge->doorSwitch->getValue())%2 == 1) && !triggered)
		{
			str = "fanOn";
			strcpy(c, str.c_str());
			fridge->sendData(fridge->sockfd, c);
			str = "peltierOn";
			strcpy(c, str.c_str());
			fridge->sendData(fridge->sockfd, c);
			triggered = 1;
		}
		else if ((stoi(fridge->tempSensor1->getValue()) <= 300) && (stoi(fridge->doorSwitch->getValue())%2 == 0) && triggered)
		{
			time(&fridge->timeStart);
			str = "fanOff";
			strcpy(c, str.c_str());
			fridge->sendData(fridge->sockfd, c);
			str = "peltierOff";
			strcpy(c, str.c_str());
			fridge->sendData(fridge->sockfd, c);
			triggered = 0;
		}
		
		if ((time(nullptr) - fridge->timeStart) >= 5 && (stoi(fridge->tempSensor1->getValue()) <= 300) && (stoi(fridge->doorSwitch->getValue()) % 2 == 0) && !triggered2)
		{
			cout << "Koelkast zit te ontdooien!" << endl;
			triggered2 = 1;
		}
		else if ((stoi(fridge->doorSwitch->getValue())%2 == 1) && triggered2)
		{
			cout << "Deurtje is weer dicht hoor!" << endl;
			triggered2 = 0;
		}
	}	

	printf("Exit fridge thread\n\r");	
	pthread_exit(NULL);

	return 0;
}

void Fridge::start()
{
	pthread_mutex_lock(&qlock);	
	if (pthread_create(&(this->tid), 
		NULL,
		Fridge::deviceThread,
		(void *)this) < 0)
	{
		pthread_mutex_unlock(&qlock);	
		cerr << "Error creating fridge thread!" << endl;
		return;
	}
	pthread_mutex_unlock(&qlock);	
}