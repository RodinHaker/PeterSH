$(document).ready(function(){
    $("#test").load("../php/load-sensors.php");
    $("#lights").load("../php/checkActuatorStatus.php");
    const intervalId = window.setInterval(function () {
        $("#test").load("../php/load-sensors.php");
        //$("#lights").load("../php/checkActuatorStatus.php");
    }, 1000);

    window.onload=function() {
        document.getElementById('bedLed').addEventListener('change', (e) => {
            $.ajax({
                type: "POST",
                url: "../php/bedLedTrigger.php"
            });
        })
    }
});
