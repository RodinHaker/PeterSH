#pragma once

#include <ctime>
#include <iostream>
#include "Device.h"

using namespace std;

class Bed : public Device
{
public:
	Bed(string name, int sockfd);
	~Bed();
	void start();
private:
	static void *deviceThread(void *device);

	Sensor* forceSensor;
	Sensor* pushButton;
	Actuator* led;
	time_t currentTime;
	bool sleeping = 0;
	bool alarm = 0;
};