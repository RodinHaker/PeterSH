/**
 * Creates a client, connects to the server and send/receives some data.
 *
 * @file client.c
 * @authors PJSDV Class D.1 Group 2
 * @version 1.0 27/11/2020
*/
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <netdb.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>

/**
 * Prints error message, exists the program and reports an abnormal termination.
 *
 * @param msg The error message to print
 */
void error(char *msg) {
    perror(msg);
    exit(0);
}

/**
 * Sends data to the server.
 *
 * @param newSocket The socket to send data to.
 * @param data The data to be sent.
 */
void sendData(int newSocket, int x) {
    int n;

    char buffer[32];
    sprintf(buffer,"%d\n", x);
    if((n = write( newSocket, buffer, strlen(buffer))) < 0)
        error("ERROR writing to socket");
    buffer[n] = '\0';
}

/**
 * Receives data from the server.
 *
 * @param newSocket The socket to receive data from.
 * @return The received data.
 */
int getData(int newSocket) {
    char buffer[32];
    int n;

    if((n = read(newSocket,buffer,31)) < 0)
        error("ERROR reading from socket");
    buffer[n] = '\0';
    return atoi(buffer);
}

/**
 * Boots the client, connects to the server and sends/receives some data.
 */
int main(int argc, char *argv[])
{
    int newSocket, portno = 8888, n;
    char serverIp[] = "192.168.178.137";
    struct sockaddr_in serv_addr;
    struct hostent *server;
    char buffer[256];
    int data;

    if (argc < 3) {
        printf("Contacting %s on port %d\n", serverIp, portno);
    }
    // create socket
    if((newSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        error("ERROR opening socket");
    // Get server host
    if((server = gethostbyname(serverIp)) == NULL)
        error("ERROR, no such host\n");

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
    serv_addr.sin_port = htons(portno);
    // Connect to server
    if(connect(newSocket,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
        error("ERROR connecting");

    // Send data to be multiplied (0 to 1000)
    for(n = 0; n < 1000; n++) {
        sendData(newSocket, n);
        data = getData(newSocket);
        printf("%d ->  %d\n",n, data);
    }
    // Send -1 to close connection
    sendData(newSocket, -1);

    // Close socket
    close(newSocket);
    //Shut down program
    return 0;
}
