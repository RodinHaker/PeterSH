#include "Wall.h"

Wall::Wall(string name, int sockfd) 
	: Device(name, sockfd)
{
	lightSensor = new Sensor("wallLightSensor", "0");
	dimSwitch = new Sensor("wallPotMeter", "0");
	RGBLed = new Actuator("wallLed", "0");
	LCDPanel = new Actuator("wallLCD", "0");
	addSensor(lightSensor);
	addSensor(dimSwitch);
	addActuator(RGBLed);	
	addActuator(LCDPanel);
}

Wall::~Wall() {
	delete lightSensor;
	delete dimSwitch;
	delete RGBLed;
	delete LCDPanel;
}


void *Wall::deviceThread(void *device)
{
	Wall *wall = (Wall *)device;
	cout << "Wall thread " << (int)wall->tid << " starting...\n" << endl;
	char* data;
	int triggered1 = 0, triggered2 = 0;
	while (1) {	
		wall->connection(wall);
		
		string str;
		char c[20];
		if ((stoi(wall->lightSensor->getValue()) > 400) && !triggered1)
		{
			str = "lcdOn";
			strcpy(c, str.c_str());
			wall->sendData(wall->sockfd, c);
			triggered1 = 1;
		}
		else if ((stoi(wall->lightSensor->getValue()) <= 400) && triggered1)
		{
			str = "lcdOff";
			strcpy(c, str.c_str());
			wall->sendData(wall->sockfd, c);
			triggered1 = 0;
		}
		
		if ((stoi(wall->dimSwitch->getValue()) > 10) && !triggered2)
		{
			str = "ledOn";
			strcpy(c, str.c_str());
			wall->sendData(wall->sockfd, c);
			triggered2 = 1;
		}
		else if ((stoi(wall->dimSwitch->getValue()) <= 10) && triggered2)
		{
			str = "ledOff";
			strcpy(c, str.c_str());
			wall->sendData(wall->sockfd, c);
			triggered2 = 0;
		}
	}	

	printf("Exit chair thread\n\r");	
	pthread_exit(NULL);

	return 0;
}

void Wall::start()
{
	pthread_mutex_lock(&qlock);	
	if (pthread_create(&(this->tid), 
		NULL,
		Wall::deviceThread,
		(void *)this) < 0)
	{
		pthread_mutex_unlock(&qlock);	
		cerr << "Error creating wall thread!" << endl;
		return;
	}
	pthread_mutex_unlock(&qlock);	
}