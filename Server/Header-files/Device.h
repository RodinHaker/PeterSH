#pragma once

#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <iostream>
#include <list>
#include <map>
#include <string>
#include <string.h>
#include <unistd.h>
#include <signal.h>

#include "Actuator.h"
#include "Sensor.h"

#define MSG_SIZE 2048

using namespace std;

/**
 * Manages device session
*/
class Device
{
public:
	Device(string name, int sockfd);
	virtual ~Device();
	virtual void start() = 0;
	void sendData(int socket, char data[]);
	char* getData(int socket);
	void writeGPIO(const char filename[], const char value[]);
	void addActuator(Actuator* actuator);
	void addSensor(Sensor* sensor);
	void setName(string name);
	const list<Actuator*> &getActuators();
	const list<Sensor*> &getSensors();
	void getDeviceAdres();
	char* getIP();
	int getPort();
	void setSocketAddr(struct sockaddr device_addr);
	sighandler_t sigpipe_handler();
	virtual void connection(Device *device);

	int sockfd;	
	int socket_OK;
	pthread_t tid;
	pthread_mutex_t qlock = PTHREAD_MUTEX_INITIALIZER;
private:
	struct sockaddr socket_addr;
	
	static void *deviceThread(void *device) {return 0;}

	char ip[INET6_ADDRSTRLEN];
	string name;
	int port;
	
	list<Actuator*> actuators;
	list<Sensor*> sensors;
};