#include "TableLamp.h"

TableLamp::TableLamp(string name, int sockfd) 
	: Device(name, sockfd)
{
	motionSensor = new Sensor("tableLampMotion", "0");
	RGBLed = new Actuator("tableLampLed", "0");
	addSensor(motionSensor);	
	addActuator(RGBLed);
}

TableLamp::~TableLamp() {
	delete motionSensor;
	delete RGBLed;
}


void *TableLamp::deviceThread(void *device)
{
	TableLamp *tableLamp = (TableLamp *)device;
	cout << "Table Lamp thread " << (int)tableLamp->tid << " starting...\n" << endl;
	char* data;
	int triggered1 = 0, triggered2 = 0;
	while (1) {	
		tableLamp->connection(tableLamp);
		string str;
		char c[20];
		if ((stoi(tableLamp->motionSensor->getValue()) == 1) && !triggered1)
		{
			str = "ledOn";
			strcpy(c, str.c_str());
			tableLamp->sendData(tableLamp->sockfd, c);
			triggered1 = 1;
		}
		else if ((stoi(tableLamp->motionSensor->getValue()) == 0) && triggered1)
		{
			str = "ledOff";
			strcpy(c, str.c_str());
			tableLamp->sendData(tableLamp->sockfd, c);
			triggered1 = 0;
		}
	}	

	printf("Exit chair thread\n\r");	
	pthread_exit(NULL);

	return 0;
}


void TableLamp::start()
{
	pthread_mutex_lock(&qlock);	
	if (pthread_create(&(this->tid), 
		NULL,
		TableLamp::deviceThread,
		(void *)this) < 0)
	{
		pthread_mutex_unlock(&qlock);	
		cerr << "Error creating table lamp thread!" << endl;
		return;
	}
	pthread_mutex_unlock(&qlock);	
}