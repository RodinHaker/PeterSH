#pragma once

#include <iostream>

using namespace std;

class Sensor
{
public:
	Sensor(string key, string value);
	void setValue(string value);
	const string &getValue();
	const string &getKey();
private:
	string key;
	string value = 0;
};