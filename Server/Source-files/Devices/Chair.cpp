#include "Chair.h"

Chair::Chair(string name, int sockfd) 
	: Device(name, sockfd)
{
	forceSensor = new Sensor("chairForce", "0");
	pushButton = new Sensor("chairButton", "0");
	led = new Actuator("chairLed", "0");
	vibrationMotor = new Actuator("chairVibration", "0");
	addSensor(forceSensor);
	addSensor(pushButton);	
	addActuator(led);
	addActuator(vibrationMotor);
}

Chair::~Chair() {
	delete forceSensor;
	delete pushButton;
	delete led;
	delete vibrationMotor;
}

void *Chair::deviceThread(void *device)
{
	Chair *chair = (Chair *)device;
	cout << "Chair thread " << (int)chair->tid << " starting...\n" << endl;
	char* data;
	int triggered1 = 0, triggered2 = 0, triggered3 = 0;;
	while (1) {	
		chair->connection(chair);
		
		string str;
		char c[20];
		if ((stoi(chair->forceSensor->getValue()) > 500) && !triggered1)
		{
			time(&chair->timeStart);
			triggered1 = 1;
		}
		else if ((stoi(chair->forceSensor->getValue()) <= 500) && triggered1)
		{
			triggered1 = 0;
		}
		
		if ((time(nullptr) - chair->timeStart) >= 5 && (stoi(chair->forceSensor->getValue()) > 500) && !triggered3)
		{
			str = "vibrationOn";
			strcpy(c, str.c_str());
			chair->sendData(chair->sockfd, c);
			triggered3 = 1;
		}
		else if ((stoi(chair->forceSensor->getValue()) <= 500) && triggered3)
		{
			str = "vibrationOff";
			strcpy(c, str.c_str());
			chair->sendData(chair->sockfd, c);
			triggered3 = 0;
		}
		
	if ((stoi(chair->pushButton->getValue()) == 1) && !triggered2)
		{
			str = "ledOn";
			strcpy(c, str.c_str());
			chair->sendData(chair->sockfd, c);
			triggered2 = 1;
		}
		else if ((stoi(chair->pushButton->getValue()) <= 500) && triggered2)
		{
			str = "ledOff";
			strcpy(c, str.c_str());
			chair->sendData(chair->sockfd, c);
			triggered2 = 0;
		}
	}	

	printf("Exit chair thread\n\r");	
	pthread_exit(NULL);

	return 0;
}

void Chair::start()
{
	pthread_mutex_lock(&qlock);	
	if (pthread_create(&(this->tid), 
		NULL,
		Chair::deviceThread,
		(void *)this) < 0)
	{
		pthread_mutex_unlock(&qlock);	
		cerr << "Error creating chair thread!" << endl;
		return;
	}
	pthread_mutex_unlock(&qlock);	
}