#pragma once

#include <ctime>
#include <iostream>
#include "Device.h"

using namespace std;

class Wall : public Device
{
public:
	Wall(string name, int sockfd);
	virtual ~Wall();
	void start();
private:
	static void *deviceThread(void *device);
	Sensor* lightSensor;
	Sensor* dimSwitch;
	Actuator* RGBLed;
	Actuator* LCDPanel;
};