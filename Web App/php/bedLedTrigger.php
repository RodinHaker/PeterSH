<?php
include 'db_con.php';

$sql = "SELECT * FROM actuators WHERE vkey='bedLed'";
$result = mysqli_query($conn, $sql);
if(mysqli_num_rows($result) > 0){
    $trigger = 0;
    while($row = mysqli_fetch_assoc($result)){
        if($row[value] == 0){
            $trigger = 1;
        }else{
            $trigger = 0;
        }
    }
    $sql = "UPDATE actuators SET value='$trigger' WHERE vkey='bedLed'";
    if ($conn->query($sql) === TRUE) {
        echo "Record updated successfully";
    } else {
        echo "Error updating record: " . $conn->error;
    }
}else{
    echo "There are no sensors!";
}