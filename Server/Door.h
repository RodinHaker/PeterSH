#pragma once

#include <ctime>
#include <iostream>
#include "Device.h"

using namespace std;

class Door : public Device
{
public:
	Door(string name, int sockfd);
	virtual ~Door();
	void start();
private:
	static void *deviceThread(void *device);
	Sensor* pushButton1;
	Sensor* pushButton2;
	Actuator* led1;
	Actuator* led2;
	Actuator* servo;
};