#include "Door.h"

Door::Door(string name, int sockfd) 
	: Device(name, sockfd)
{
	pushButton1 = new Sensor("doorButton1", "0");
	pushButton2 = new Sensor("doorButton2", "0");
	led1 = new Actuator("doorLed1", "0");
	led2 = new Actuator("doorLed2", "0");
	servo = new Actuator("doorServo", "0");
	addSensor(pushButton1);
	addSensor(pushButton2);	
	addActuator(led1);	
	addActuator(led2);
	addActuator(servo);
}

Door::~Door() {
	delete pushButton1;
	delete pushButton2;
	delete led1;
	delete led2;
	delete servo;
}

void *Door::deviceThread(void *device)
{
	Door *door = (Door *)device;
	cout << "Door thread " << (int)door->tid << " starting...\n" << endl;
	char* data;
	int triggered1 = 0, triggered2 = 0;
	while (1) {	
		door->connection(door);
		
		string str;
		char c[20];
		if ((stoi(door->pushButton1->getValue()) == 1) && !triggered1)
		{
			str = "ledsOn";
			strcpy(c, str.c_str());
			door->sendData(door->sockfd, c);
			str = "doorOpen";
			strcpy(c, str.c_str());
			door->sendData(door->sockfd, c);
			triggered1 = 1;
		}
		else if ((stoi(door->pushButton1->getValue()) == 0) && triggered1)
		{
			str = "ledsOff";
			strcpy(c, str.c_str());
			door->sendData(door->sockfd, c);
			str = "doorClose";
			strcpy(c, str.c_str());
			door->sendData(door->sockfd, c);
			triggered1 = 0;
		}
	}	

	printf("Exit chair thread\n\r");	
	pthread_exit(NULL);

	return 0;
}

void Door::start()
{
	pthread_mutex_lock(&qlock);	
	if (pthread_create(&(this->tid), 
		NULL,
		Door::deviceThread,
		(void *)this) < 0)
	{
		pthread_mutex_unlock(&qlock);	
		cerr << "Error creating door thread!" << endl;
		return;
	}
	pthread_mutex_unlock(&qlock);	
}