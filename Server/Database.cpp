#include "Database.h"

Database::Database()
{
	devices = new list<Device*>;
	host = "localhost";
	uname = "pi";
	passwd = "07071999";
	database = "PeterSH";
	driver = NULL;
	con = NULL;
	stmt = NULL;
	res = NULL;
	this->createConnection();
	this->createDatabase();
}

Database::~Database() 
{
	delete res;
	delete stmt;
	delete con;
}

void Database::createConnection()
{
	try {
		driver = get_driver_instance();
		con = driver->connect(host, uname, passwd);
		con->setSchema(database);
	} catch(SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}
}

void Database::createDatabase()
{
	try {
		stmt = con->createStatement();
		stmt->execute("CREATE TABLE IF NOT EXISTS sensors(vkey VARCHAR(30) NOT NULL, value INT NOT NULL, PRIMARY KEY(vkey));");
		stmt->execute("CREATE TABLE IF NOT EXISTS actuators(vkey VARCHAR(30) NOT NULL, value INT NOT NULL, PRIMARY KEY(vkey));");
	}catch (SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}
}

void Database::addDevice(Device *device)
{
	devices->push_back(device);
}

void Database::updateDatabase()
{
	stmt = con->createStatement();
	for (list<Device*>::iterator device = devices->begin(); device != devices->end(); ++device)
	{
		list<Sensor*> sensors = (*device)->getSensors();
		list<Actuator*> actuators = (*device)->getActuators();
		for (list<Sensor*>::iterator sensor = sensors.begin(); sensor != sensors.end(); ++sensor)
		{
			string key = (*sensor)->getKey();
			string value = (*sensor)->getValue();
			updateSensorData(key, value);
		}
		for (list<Actuator*>::iterator actuator = actuators.begin(); actuator != actuators.end(); ++actuator)
		{
			string key = (*actuator)->getKey();
			string value = (*actuator)->getValue();
			updateActuatorData(key, value);
		}
	}
}

void Database::updateSensorData(string key, string value) {
	try {
		stmt = con->createStatement();
		stmt->execute("INSERT INTO sensors(vkey, value) VALUES(\"" + key + "\", \"" + value + "\") ON DUPLICATE KEY UPDATE value=\"" + value + "\"");
	}
	catch (SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}
}

void Database::updateActuatorData(string key, string value) {
	try {
		stmt = con->createStatement();
		stmt->execute("INSERT INTO actuators(vkey, value) VALUES(\"" + key + "\", \"" + value + "\") ON DUPLICATE KEY UPDATE value=\"" + value + "\"");
	}
	catch (SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}
}

list<Device*> *Database::getDevices()
{
	return devices;
}

string Database::readActuatorData(string key) {
	return "";
}