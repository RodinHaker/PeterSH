
#include "Server.h"

Server::Server(Database *database)
	: database(database)
{
}

//void *Server::cliThread(void *server)
//{
//	Server *serv = (Server *)server;
//	
//	while (1)
//	{
//		char * line = readline("> ");
//		if (!line) break;
//		if (*line) add_history(line);
//		
//		if (strcmp(line, "quit"))
//		{
//			pthread_mutex_lock(&serv->mlock);	
//			serv->quit = true;
//			pthread_mutex_unlock(&serv->mlock);	
//			free(line);
//			break;
//		}
//			
//		
//		free(line);
//	}
//	
//	cout << "CLI thread ended..." << endl;
//	pthread_exit(NULL);
//	return 0;
//}

void *Server::databaseThread(void *server)
{
	Server *serv = (Server *)server;
	while (1)
	{
		//pthread_mutex_lock(&serv->mlock);
		serv->database->updateDatabase();
		//pthread_mutex_unlock(&serv->mlock);
		sleep(1);
	}
}

void *Server::socketThread(void *server)
{
	Server *serv = (Server *)server;
	list<Device*>::iterator iter;
	
	serv->quit = false;
	
	int sockaddr_in_size = sizeof(struct sockaddr_in);
	
	int device_sockfd;
	struct sockaddr device_addr;
	
	pthread_t tid;

	serv->sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (serv->sockfd == -1)
	{
		cerr << "Error creating socket..." << endl;
		pthread_exit(NULL);
		return 0;
	}
	
	serv->sock_addr.sin_family = AF_INET;
	serv->sock_addr.sin_port = htons(serv->portnm);
	serv->sock_addr.sin_addr.s_addr = INADDR_ANY;
	
	memset(serv->sock_addr.sin_zero, '\0', sizeof serv->sock_addr.sin_zero);

	if (bind(serv->sockfd,
		(struct sockaddr *)&serv->sock_addr,
		sizeof(serv->sock_addr)) < 0) {
		cerr << "Error binding socket..." << endl;
		pthread_exit(NULL);
		return 0;
	}
	
	if (listen(serv->sockfd, LISTEN_BACKLOG) == -1) {
		cerr << "Error listening..." << endl;
		pthread_exit(NULL);
		return 0;
	}
	cout << "Waiting for incoming connections..." << endl;
	while (1) {
		if ((device_sockfd = accept(
			serv->sockfd,
			(struct sockaddr *)&(device_addr),
			(socklen_t *)&sockaddr_in_size)))
		{
			pthread_mutex_lock(&serv->mlock);
			switch (device_addr.sa_data[5]) {
				case 109: {
					Bed *bed = new Bed("bed", device_sockfd);
					bed->setSocketAddr(device_addr);
					bed->start();
					serv->database->addDevice(bed);
					cout << "New connection from " << bed->getIP() << ":" << bed->getPort() << endl;
					pthread_mutex_unlock(&serv->mlock);
					break;
				}
				case 110: {
					Column *column = new Column("column", device_sockfd);
					column->setSocketAddr(device_addr);
					column->start();
					serv->database->addDevice(column);
					cout << "New connection from " << column->getIP() << ":" << column->getPort() << endl;
					pthread_mutex_unlock(&serv->mlock);
					break;
				}
				case 111: {
					Chair *chair = new Chair("chair", device_sockfd);
					chair->setSocketAddr(device_addr);
					chair->start();
					serv->database->addDevice(chair);
					cout << "New connection from " << chair->getIP() << ":" << chair->getPort() << endl;
					pthread_mutex_unlock(&serv->mlock);
					break;
				}
				case 112: {
					Fridge *fridge = new Fridge("fridge", device_sockfd);
					fridge->setSocketAddr(device_addr);
					fridge->start();
					serv->database->addDevice(fridge);
					cout << "New connection from " << fridge->getIP() << ":" << fridge->getPort() << endl;
					pthread_mutex_unlock(&serv->mlock);
					break;
				}
				case 113: {
					Wall *wall = new Wall("wall", device_sockfd);
					wall->setSocketAddr(device_addr);
					wall->start();
					serv->database->addDevice(wall);
					cout << "New connection from " << wall->getIP() << ":" << wall->getPort() << endl;
					pthread_mutex_unlock(&serv->mlock);
					break;
				}
				case 114: {
					Door *door = new Door("door", device_sockfd);
					door->setSocketAddr(device_addr);
					door->start();
					serv->database->addDevice(door);
					cout << "New connection from " << door->getIP() << ":" << door->getPort() << endl;
					pthread_mutex_unlock(&serv->mlock);
					break;
				}
				case 115: {
					TableLamp *tableLamp = new TableLamp("tableLamp", device_sockfd);
					tableLamp->setSocketAddr(device_addr);
					tableLamp->start();
					serv->database->addDevice(tableLamp);
					cout << "New connection from " << tableLamp->getIP() << ":" << tableLamp->getPort() << endl;
					pthread_mutex_unlock(&serv->mlock);
					break;
				}
			}
		}
		if (serv->quit) {
			break;
		}
	}
	
	// TODO: own function for ending the server, disconnect all clients, join&exit all threads etc.
	for(iter = serv->database->getDevices()->begin() ; iter != serv->database->getDevices()->end() ; ++iter)
	{
		pthread_join((*iter)->tid, NULL);
	}
	
	cout << "Communication thread ended..." << endl;
	pthread_exit(NULL);
	return 0;
}

void Server::start()
{
	cout << "Starting server on 10.0.42.1:8888" << endl;
	
	pthread_create(&this->database_thread, NULL, Server::databaseThread, (void *)this);
	pthread_create(&this->thread, NULL, Server::socketThread, (void *)this);
	pthread_exit(NULL);
};

pthread_t Server::getThread()
{
	return thread;
}
