#include "Device.h"

Device::Device(string name, int sockfd)
	: sockfd(sockfd)
	, name(name)
{
	socket_OK = 0;
	this->getDeviceAdres();
}

Device::~Device() {}

/**
 * Sends data to the client.
 *
 * @param newSocket The socket to send data to.
 * @param data The data to be sent.
 */
void Device::sendData(int socket, char* data) {
	int n, flags = 0;
	char* newData = new char[strlen(data)+2];
	strcpy(newData, data);
	strcat(newData, "\n");
	if ((n = send(socket, newData, strlen(newData), flags)) < 0)
		cerr << "ERROR writing to socket" << endl;
	data[n] = '\0';
	delete[] newData;
}

/**
 * Receives data from the client.
 *
 * @param newSocket The socket to receive data from.
 * @return The received data.
 */
char* Device::getData(int socket) {
	char *buffer = new char[MSG_SIZE], *token = new char[MSG_SIZE];
	int n, flags = 0;
	
	if ((n = recv(socket, buffer, MSG_SIZE, 0)) < 0) {
		cerr << "ERROR reading from socket" << endl;
	}else if (n == 0) {
		string str = "disconnected\0";
		strcpy(buffer, str.c_str());
		return buffer;
	}
	token = strtok(buffer, "\n");
	//token[n] = '\0';
	return buffer;
}

void Device::writeGPIO(const char filename[], const char value[]) {
	FILE* fp;                           // create a file pointer fp
	fp = fopen(filename, "w+");         // open file for writing
	fprintf(fp, "%s", value);           // send the value to the file
	fclose(fp);                         // close the file using fp
}

void Device::start()
{
	if (pthread_create(&(this->tid), 
		NULL,
		Device::deviceThread,
		(void *)this) < 0)
	{
		cerr << "Error creating session thread!" << endl;
		return;
	}
}

void Device::addActuator(Actuator* actuator)
{
	actuators.push_back(actuator);
}

void Device::addSensor(Sensor* sensor) 
{
	sensors.push_back(sensor);
}

void Device::setName(string name)
{
	this->name = name;
}

const list<Actuator*> &Device::getActuators()
{
	return actuators;
}

const list<Sensor*> &Device::getSensors()
{
	return sensors;
}

void Device::getDeviceAdres()
{
	socklen_t socket_len;
	struct sockaddr_storage socket_addr;
	
	socket_len = sizeof socket_addr;
	getpeername(this->sockfd, (struct sockaddr *)&socket_addr, &socket_len);
	
	if (socket_addr.ss_family == AF_INET)
	{
		struct sockaddr_in *s = (struct sockaddr_in *)&socket_addr;
		this->port = ntohs(s->sin_port);
		inet_ntop(AF_INET, &s->sin_addr, this->ip, sizeof(this->ip));
	}
}

char* Device::getIP()
{
	return ip;
}

int Device::getPort()
{
	return port;
}

void Device::setSocketAddr(struct sockaddr device_addr) 
{ 
	socket_addr = device_addr;
}

sighandler_t Device::sigpipe_handler()
{
	printf("SIGPIPE caught\n");
	socket_OK = 0;
}

void Device::connection(Device *device)
{
	pthread_mutex_lock(&device->qlock);	
	char *data, *token, *subtoken;
	char *saveptr1, *saveptr2;
	data = device->getData(device->sockfd);
	if (data == NULL)
		return;
	if (strcasecmp(data, "disconnected") == 0) {
		device->socket_OK = 0;
		return;
	}
	for (token = strtok_r(data, ";", &saveptr1); token != NULL; token = strtok_r(NULL, ";", &saveptr1))
	{
		int found = 0;
		if (token == NULL)
			continue;
		subtoken = strtok_r(token, ",", &saveptr2);
		if (subtoken == NULL)
			continue;
		const list<Sensor*> &tempS = device->getSensors();
		for (list<Sensor*>::const_iterator sensor = tempS.begin(); sensor != tempS.end(); ++sensor) {
			if (strcmp(subtoken, (*sensor)->getKey().c_str()) == 0)
			{
				subtoken = strtok_r(NULL, ",", &saveptr2);
				found = 1;
				if (subtoken == NULL)
					break;
				(*sensor)->setValue(subtoken);
				break;
			}
		}
		if (!found) {
			const list<Actuator*> &tempA = device->getActuators();
			for (list<Actuator*>::const_iterator actuator = tempA.begin(); actuator != tempA.end(); ++actuator) {
				if (strcmp(subtoken, (*actuator)->getKey().c_str()) == 0)
				{
					subtoken = strtok_r(NULL, ",", &saveptr2);
					if (subtoken == NULL)
						break;
					(*actuator)->setValue(subtoken);
					break;
				}
			}
		}
	}
	pthread_mutex_unlock(&device->qlock);	
}