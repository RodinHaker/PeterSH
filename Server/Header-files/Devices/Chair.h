#pragma once

#include <ctime>
#include <iostream>
#include "Device.h"

using namespace std;

class Chair : public Device
{
public:
	Chair(string name, int sockfd);
	virtual ~Chair();
	void start();
private:
	static void *deviceThread(void *device);
	Sensor* forceSensor;
	Sensor* pushButton;
	Actuator* led;
	Actuator* vibrationMotor;
	time_t timeStart;
};